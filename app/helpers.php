<?php

/**
 * Return "active"
 *
 * @param string $route
 * @return string
 */
function setActive($route)
{
    return request()->routeIs($route) ? 'active' : '';
}