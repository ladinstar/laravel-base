<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Kodeine\Metable\Metable;

class Post extends Model
{

    use Metable;

    use SoftDeletes;

    protected $metaTable = 'posts_meta';

    protected $dates = [
        'deleted_at',
    ];

    public function author()
    {
        return $this->belongsTo(User::class);
    }

    public function parent()
    {
        return $this->belongsTo(Post::class, 'parent_id');
    }

    public function childs()
    {
        return $this->hasMany(Post::class, 'parent_id');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

}
