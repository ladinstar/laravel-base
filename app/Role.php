<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function permissions() {
    	return $this->belongsToMany(Permission::class,'roles_permissions');
    }

	public function hasPermission($permission) {
		return (bool) $this->permissions->where('slug', $permission->slug)->count();
	}

	public function users() {
		return $this->belongsToMany(User::class,'users_roles');
	}
}