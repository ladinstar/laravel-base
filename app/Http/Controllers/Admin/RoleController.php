<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\RoleRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Role;
use App\Permission;
use Toastr;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::paginate(10);
        return view('admin.pages.roles.index',compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $role = new Role();
        $permissions = Permission::all();
        return view('admin.pages.roles.form', compact('role','permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoleRequest $request)
    {

        $role = new Role();
        $role->slug = Str::slug($request->slug);
        $role->name = $request->name;
        $role->description = $request->description;

        $role->save();

        $role->permissions()->attach($request->permissions);

        Toastr::success('Le rôle a été crée avec succès !');

        return redirect()->route('admin.roles.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::find($id);
        $permissions = Permission::all();
        if(!$role){
            Toastr::error('Ce rôle n\'existe pas ou a été supprimé');
            return redirect()->route('admin.roles.index');
        }
        return view('admin.pages.roles.form', compact('role','permissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'slug' => 'required|string|max:255|unique:roles,slug,'.$id,
            'name' => 'required|string|max:255',
            'description' => 'string',
            'permissions' => 'required|array',
        ];
        $request->validate($rules);

        $role = Role::find($id);

        $role->slug = Str::slug($request->slug);
        $role->name = $request->name;
        $role->description = $request->description;

        $role->save();

        $role->permissions()->detach();

        $role->permissions()->attach($request->permissions);

        Toastr::success('Le role a été modifié avec succès !');

        return redirect()->route('admin.roles.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->middleware('role:superadmin');
        $role = Role::find($id);
        $role->permissions()->detach();

        Toastr::success('Ce rôle a été supprimé avec succès !');

        if(!$role->delete()){
            Toastr::error('Une erreur s\'est produite lors de la suppression de cet utilisateur');
        }

        return redirect()->route('admin.roles.index');
    }
}
