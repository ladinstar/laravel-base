<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Role;
use Toastr;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::withTrashed()->paginate(10);
        return view('admin.pages.users.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = new User();
        $roles = Role::all();
        return view('admin.pages.users.form', compact('user','roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {

        $role = Role::find($request->role);
        $permissions = $role->permissions;

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);

        $user->save();

        $user->roles()->attach($role);
        foreach ($permissions as $permission) {
            $user->permissions()->attach($permission);
        }

        Toastr::success('L\'utilisateur a été crée avec succès !');

        return redirect()->route('admin.users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::all();
        if(!$user){
            Toastr::error('Cet utilisateur n\'existe pas ou a été supprimé');
            return redirect()->route('admin.users.index');
        }
        return view('admin.pages.users.form', compact('user','roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'required|string|max:255',
            'username' => 'required|string|min:6|max:255|unique:users,username,'.$id,
            'email' => 'required|string|email|max:255|unique:users,email,'.$id,
            'role' => 'required|exists:roles,id',
            ];
        if($request->password){
            $rules['password'] = 'required|string|min:8|confirmed';
        }

        $request->validate($rules);

        $user = User::find($id);

        $user->name = $request->name;
        $user->email = $request->email;
        if($request->password){
            $user->password = Hash::make($request->password);
        }

        $user->save();

        $role = Role::find($request->role);
        $permissions = $role->permissions;

        $user->permissions()->detach();
        $user->roles()->detach();

        $user->roles()->attach($role);

        foreach ($permissions as $permission) {
            $user->permissions()->attach($permission);
        }

        Toastr::success('L\'utilisateur a été modifié avec succès !');

        return redirect()->route('admin.users.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->middleware('role:superadmin');

        $user = User::find($id);
        
        Toastr::success('Cet utilisateur a été supprimé avec succès !');

        if(!$user->delete()){
            Toastr::error('Une erreur s\'est produite lors de la suppression de cet utilisateur');
        }

        return redirect()->route('admin.users.index');
    }

    public function active($id){
        $this->middleware('role:superadmin');

        $user = User::withTrashed()->where('id', $id)->first();

        if(!$user->restore()){
            Toastr::error('Une erreur s\'est produite lors de la réactivation de cet utilisateur');
        }
        else{
            Toastr::success('Cet utilisateur a été réactivé avec succès !');
        }

        return redirect()->route('admin.users.index');
    }
}
