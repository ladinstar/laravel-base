<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\PermissionRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Permission;
use Toastr;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $permissions = Permission::paginate(10);
        return view('admin.pages.permissions.index',compact('permissions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permission = new Permission();
        return view('admin.pages.permissions.form', compact('permission'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PermissionRequest $request)
    {

        $permission = new Permission();
        $permission->slug = Str::slug($request->slug);
        $permission->name = $request->name;
        $permission->description = $request->description;

        $permission->save();

        Toastr::success('La permission a été crée avec succès !');

        return redirect()->route('admin.permissions.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $permission = Permission::find($id);

        if(!$permission){
            Toastr::error('Cette permission n\'existe pas ou a été supprimée');
            return redirect()->route('admin.permissions.index');
        }
        return view('admin.pages.permissions.form', compact('permission'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'slug' => 'required|string|max:255|unique:permissions,slug,'.$id,
            'name' => 'required|string|max:255',
            'description' => 'string',
        ];
        $request->validate($rules);

        $permission = Permission::find($id);
        if(!$permission){
            Toastr::error('Cette permission n\'existe pas ou a été supprimée');
            return redirect()->route('admin.permissions.index');
        }

        $permission->slug = Str::slug($request->slug);
        $permission->name = $request->name;
        $permission->description = $request->description;

        $permission->save();

        Toastr::success('La permission a été modifiée avec succès !');

        return redirect()->route('admin.permissions.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->middleware('role:superadmin');

        $permission = Permission::find($id);
        if(!$permission->delete()){
            Toastr::error('Une erreur s\'est produite lors de la suppression de la permission');
        }
        else{
            Toastr::success('Cette permission a été supprimée avec succès !');
        }

        return redirect()->route('admin.permissions.index');
    }
}
