<?php

namespace App\Http\Middleware;

use Closure;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param $role
     * @param null $permission
     *
     * @return mixed
     */
    public function handle($request, Closure $next, ... $roles)
    {
        /**
         * Fix for "Call to a member function hasRole() on null".
         * If the user is not logged in, there is no user data to process,
         * so we need to throw 404 code.
         */
        if(is_null($request->user())){
            return redirect()->route('login');
        }

        foreach($roles as $role) {
            // Check if user has the role This check will depend on how your roles are set up
            if($request->user()->hasRole($role))
                return $next($request);
        }
        
        return redirect()->back();

    }
}
