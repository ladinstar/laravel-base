<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Kodeine\Metable\Metable;

class Comment extends Model
{

    use Metable;

    use SoftDeletes;

    protected $metaTable = 'comments_meta';

    protected $dates = [
        'deleted_at',
    ];

    public function author()
    {
        return $this->belongsTo(User::class);
    }

    public function parent()
    {
        return $this->belongsTo(Comment::class, 'parent_id');
    }

    public function childs()
    {
        return $this->hasMany(Comment::class, 'parent_id');
    }

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

}
