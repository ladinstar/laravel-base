@extends('admin.layouts.base')

@section('title','Gestion des utilisateurs')

@section('content')
<!-- Header -->
<div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
  <div class="container-fluid">
    <div class="header-body">
    </div>
  </div>
</div>
<div class="container-fluid mt--7">
  <!-- Table -->
  <div class="row">
    <div class="col">
      <div class="card shadow">
        <div class="card-header border-0">
          <div class="row">
            <div class="col-sm-6">
              <h3 class="mb-0" style="padding: 0.625rem 0;">Liste des utilisateurs</h3>
            </div>
            <div class="col-sm-6 text-right">
              <a href="{{ route('admin.users.create') }}" class="btn btn-primary"><i class="fas fa-user-plus"></i> Ajouter un utilisateur</a>
            </div>
          </div>
        </div>
        <div class="table-responsive">
          <table class="table align-items-center table-flush">
            <thead class="thead-light">
              <tr>
                <th scope="col">Utilisateur</th>
                <th scope="col">Roles</th>
                <th scope="col">Permissions</th>
                <th scope="col"></th>
              </tr>
            </thead>
            <tbody>
              @foreach($users as $user)
              <tr>
                <th scope="row">
                  <div class="media align-items-center">
                    <a href="#" class="avatar rounded-circle mr-3">
                      <img alt="Image placeholder" src="{{ asset('assets/admin/img/icons') }}/{{ $user->getMeta('picture') ?? 'user.png' }}">
                    </a>
                    <div class="media-body @if($user->trashed())token deleted @endif">
                      <span class="mb-0 text-sm">{{ $user->name }}</span>
                    </div>
                  </div>
                </th>
                <td>
                  @if($user->hasRole('superadmin'))
                  <span class="badge badge-danger @if($user->trashed())token deleted @endif">Super Administrateur</span>
                  @endif
                  @if($user->hasRole('admin'))
                  <span class="badge badge-success @if($user->trashed())token deleted @endif">Administrateur</span>
                  @endif
                  @if($user->hasRole('editor'))
                  <span class="badge badge-primary @if($user->trashed())token deleted @endif">Editeur</span>
                  @endif
                  @if($user->hasRole('user'))
                  <span class="badge badge-default @if($user->trashed())token deleted @endif">Utilisateur</span>
                  @endif
                </td>
                <td>
                  @forelse($user->permissions as $permission)
                    <span class="badge badge-info @if($user->trashed())token deleted @endif">{{ $permission->slug }}</span>
                  @empty
                    <span class="badge badge-info @if($user->trashed())token deleted @endif">Aucune permission particulière</span>
                  @endforelse
                </td>
                <td class="text-right">
                  <div class="dropdown">
                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                      @if($user->trashed())
                        @role('superadmin')
                        <a class="dropdown-item" href="{{ route('admin.users.active', $user) }}"><i class="fas fa-check text-green"></i> Réactiver</a>
                        @endrole
                      @else
                        <a class="dropdown-item" href="{{ route('admin.users.edit', $user) }}"><i class="fas fa-pencil-alt text-blue"></i> Modifier</a>
                        @role('superadmin')
                        <a class="dropdown-item delete-item" href="{{ route('admin.users.destroy', $user) }}" data-name="{{ $user->name }}" data-id="{{ $user->id }}" data-toggle="modal" data-target="#deleteModal"><i class="fas fa-trash text-red"></i> Supprimer</a>
                        @endrole
                      @endif
                    </div>
                  </div>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <div class="card-footer py-4">
            {{ $users->links() }}
        </div>
      </div>
    </div>
  </div>
  <!-- Modal -->
  <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Confirmation de suppression</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          Vous êtes sur le point de supprimer l'utilisateur <span class="badge badge-info"></span> dans la base de données. Etes-vous sûr de cela ?
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
          <form action="" method="POST">
            <input type="hidden" name="_method" value="DELETE">
            @csrf
            <button type="submit" class="btn btn-danger">Oui</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
  $(document).ready(function(){

    var userName;
    var deleteUrl;

    $('a.delete-item').click(function(e) {
      deleteUrl = $(this).attr('href');
      userName = $(this).attr('data-name');
      e.preventDefault();
    });

    $('#deleteModal').on('show.bs.modal', function (e) {
      $(this).find('.modal-body .badge').text(userName);
      $(this).find('.modal-footer form').attr('action', deleteUrl);
    });

  });
</script>
@endsection