@extends('admin.layouts.base')

@section('title','Création des utilisateurs')

@section('content')
<!-- Header -->
<div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
  <div class="container-fluid">
    <div class="header-body">
    </div>
  </div>
</div>
<div class="container mt--8 pb-5">
  <!-- Table -->
  <div class="row justify-content-center">
    <div class="col-lg-6 col-md-8">
      <div class="card bg-secondary shadow border-0">
        <div class="card-body px-lg-5 py-lg-5">
          <div class="text-center text-muted mb-4">
            <small>Création d'utilisateur</small>
          </div>
          <form role="form" action="{{ ($user->id) ? route('admin.users.update', $user) : route('admin.users.store') }}" method="POST">
            @csrf
            @if($user->id)
            <input type="hidden" name="_method" value="PUT">
            @endif
            <div class="form-group @if($errors->first('name')) has-danger @endif">
              <div class="input-group input-group-alternative mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="ni ni-hat-3"></i></span>
                </div>
                <input class="form-control  
                @if($errors->first('name')) is-invalid @endif" name="name" value="{{ old('name') ?? $user->name }}" placeholder="Nom" type="text">
              </div>
              @error('name')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
              @enderror
            </div>
            <div class="form-group @if($errors->first('username')) has-danger @endif">
              <div class="input-group input-group-alternative mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="ni ni-hat-3"></i></span>
                </div>
                <input class="form-control  
                @if($errors->first('username')) is-invalid @endif" name="username" value="{{ old('username') ?? $user->username }}" placeholder="Nom d'utilisateur" type="text">
              </div>
              @error('username')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
              @enderror
            </div>
            <div class="form-group @if($errors->first('email')) has-danger @endif">
              <div class="input-group input-group-alternative mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                </div>
                <input class="form-control @if($errors->first('email')) is-invalid @endif" name="email" value="{{ old('email') ?? $user->email }}" placeholder="Email" type="email">
              </div>
              @error('email')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
              @enderror
            </div>
            <div class="form-group">
              <label for="role">Rôle de l'utilisateur</label>
              <select id="role" class="form-control" name="role">
                @foreach($roles as $role)
                <option class="@if($errors->first('role')) is-invalid @endif" value="{{ $role->id }}" @if($user->hasRole($role->slug))selected="selected"@endif>{{ $role->name }}</option>
                @endforeach
              </select>
              @error('role')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
              @enderror
            </div>
            <div class="form-group @if($errors->first('password')) has-danger @endif">
              <div class="input-group input-group-alternative">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                </div>
                <input class="form-control @if($errors->first('password')) is-invalid @endif" name="password" placeholder="Mot de passe" type="password">
              </div>
              @error('password')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
              @enderror
            </div>
            <div class="form-group @if($errors->first('password_confirmation')) has-danger @endif">
              <div class="input-group input-group-alternative">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                </div>
                <input class="form-control @if($errors->first('password_confirmation')) is-invalid @endif" name="password_confirmation" placeholder="Retaper le mot de passe" type="password">
              </div>
              @error('password_confirmation')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
              @enderror
            </div>
            <div class="text-center">
              <button type="submit" class="btn btn-primary mt-4">Ajouter un utilisateur</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
  $(document).ready(function(){
  });
</script>
@endsection