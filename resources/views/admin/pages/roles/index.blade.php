@extends('admin.layouts.base')

@section('title','Gestion des roles')

@section('content')
<!-- Header -->
<div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
  <div class="container-fluid">
    <div class="header-body">
    </div>
  </div>
</div>
<div class="container-fluid mt--7">
  <!-- Table -->
  <div class="row">
    <div class="col">
      <div class="card shadow">
        <div class="card-header border-0">
          <div class="row">
            <div class="col-sm-6">
              <h3 class="mb-0" style="padding: 0.625rem 0;">Liste des rôles</h3>
            </div>
            <div class="col-sm-6 text-right">
              <a href="{{ route('admin.roles.create') }}" class="btn btn-primary"><i class="fas fa-user-plus"></i> Ajouter un rôle</a>
            </div>
          </div>
        </div>
        <div class="table-responsive">
          <table class="table align-items-center table-flush">
            <thead class="thead-light">
              <tr>
                <th scope="col">Slug</th>
                <th scope="col">Nom</th>
                <th scope="col">Description</th>
                <th scope="col"></th>
              </tr>
            </thead>
            <tbody>
              @foreach($roles as $role)
              <tr>
                <th scope="row">
                  <span class="badge badge-default">{{ $role->slug }}</span>
                </th>
                <td>
                  {{ $role->name }}
                </td>
                <td>
                  {{ $role->description }}
                </td>
                <td class="text-right">
                  <div class="dropdown">
                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                      <a class="dropdown-item" href="{{ route('admin.roles.edit', $role) }}"><i class="fas fa-pencil-alt text-blue"></i> Modifier</a>
                      @role('superadmin')
                      <a class="dropdown-item delete-item" href="{{ route('admin.roles.destroy', $role) }}" data-name="{{ $role->name }}" data-id="{{ $role->id }}" data-toggle="modal" data-target="#deleteModal"><i class="fas fa-trash text-red"></i> Supprimer</a>
                      @endrole
                    </div>
                  </div>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <div class="card-footer py-4">
            {{ $roles->links() }}
        </div>
      </div>
    </div>
  </div>
  <!-- Modal -->
  <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Confirmation de suppression</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          Vous êtes sur le point de supprimer le role <span class="badge badge-info"></span> dans la base de données. Etes-vous sûr de cela ?
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
          <form action="" method="POST">
            <input type="hidden" name="_method" value="DELETE">
            @csrf
            <button type="submit" class="btn btn-danger">Oui</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
  $(document).ready(function(){

    var roleName;
    var deleteUrl;

    $('a.delete-item').click(function(e) {
      deleteUrl = $(this).attr('href');
      roleName = $(this).attr('data-name');
      e.preventDefault();
    });

    $('#deleteModal').on('show.bs.modal', function (e) {
      $(this).find('.modal-body .badge').text(roleName);
      $(this).find('.modal-footer form').attr('action', deleteUrl);
    });

  });
</script>
@endsection