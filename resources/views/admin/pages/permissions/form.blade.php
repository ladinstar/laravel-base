@extends('admin.layouts.base')

@section('title','Création d\'une permission')

@section('content')
<!-- Header -->
<div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
  <div class="container-fluid">
    <div class="header-body">
    </div>
  </div>
</div>
<div class="container mt--8 pb-5">
  <!-- Table -->
  <div class="row justify-content-center">
    <div class="col-lg-6 col-md-8">
      <div class="card bg-secondary shadow border-0">
        <div class="card-body px-lg-5 py-lg-5">
          <div class="text-center text-muted mb-4">
            <small>Création d'une permission</small>
          </div>
          <form role="form" action="{{ ($permission->id) ? route('admin.permissions.update', $permission) : route('admin.permissions.store') }}" method="POST">
            @csrf
            @if($permission->id)
            <input type="hidden" name="_method" value="PUT">
            @endif
            <div class="form-group @if($errors->first('slug')) has-danger @endif">
              <label for="description">Slug</label>
              <div class="input-group input-group-alternative mb-3">
                <input class="form-control  
                @if($errors->first('slug')) is-invalid @endif" name="slug" value="{{ old('slug') ?? $permission->slug }}" placeholder="Slug de la permission" type="text">
              </div>
              @error('slug')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
              @enderror
            </div>
            <div class="form-group @if($errors->first('name')) has-danger @endif">
              <label for="description">Nom</label>
              <div class="input-group input-group-alternative mb-3">
                <input class="form-control @if($errors->first('name')) is-invalid @endif" name="name" value="{{ old('name') ?? $permission->name }}" placeholder="Nom de la permission" type="text">
              </div>
              @error('name')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
              @enderror
            </div>
            <div class="form-group @if($errors->first('description')) has-danger @endif">
              <label for="description">Description</label>
              <textarea class="form-control form-control-alternative @if($errors->first('description')) is-invalid @endif" rows="3" placeholder="Ecrivez quelque chose ici ..." name="description" id="description">{{ old('description') ?? $permission->description }}</textarea>
              @error('description')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
              @enderror
            </div>
            <div class="text-center">
              <button type="submit" class="btn btn-primary mt-4">Ajouter une permission</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
  $(document).ready(funection(){
  });
</script>
@endsection