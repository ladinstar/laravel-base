<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>
    @yield('title','Tableau de bord') | Admin
  </title>
  <!-- Favicon -->
  <link href="{{ asset('assets/admin/img/brand/favicon.png') }}" rel="icon" type="image/png">
  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
  <!-- Icons -->
  <link href="{{ asset('assets/admin/js/plugins/nucleo/css/nucleo.css') }}" rel="stylesheet" />
  <link href="{{ asset('assets/admin/js/plugins/@fortawesome/fontawesome-free/css/all.min.css') }}" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
  <!-- CSS Files -->
  <link href="{{ asset('assets/admin/css/argon-dashboard.css?v=1.1.0') }}" rel="stylesheet" />
</head>

<body class="">
  @include('admin.partials.navbar-left')
  <div class="main-content">
    @include('admin.partials.navbar')
    @yield('content')
  </div>
  <!--   Core   -->
  <script src="{{ asset('assets/admin/js/plugins/jquery/dist/jquery.min.js') }}"></script>
  <script src="{{ asset('assets/admin/js/plugins/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
  <!--   Optional JS   -->
  <script src="{{ asset('assets/admin/js/plugins/chart.js/dist/Chart.min.js') }}"></script>
  <script src="{{ asset('assets/admin/js/plugins/chart.js/dist/Chart.extension.js') }}"></script>
  <!--   Argon JS   -->
  <script src="{{ asset('assets/admin/js/argon-dashboard.min.js?v=1.1.0') }}"></script>
  <script src="{{ asset('assets/admin/js/argon.min.js?v=1.1.0') }}"></script>
  <script src="https://cdn.trackjs.com/agent/v3/latest/t.js') }}"></script>
  <script>
    window.TrackJS &&
      TrackJS.install({
        token: "ee6fab19c5a04ac1a32a645abde4613a",
        application: "argon-dashboard-free"
      });
  </script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
  {!! Toastr::message() !!}
  @yield('script')
</body>

</html>