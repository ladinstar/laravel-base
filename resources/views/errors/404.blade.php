@extends("errors::custom")

@section("title", __("The page can't be found"))
@section("code", "404")
@section("message", __("The page can't be found"))