<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<link href="{{ asset('assets/admin/img/brand/favicon.png') }}" rel="icon" type="image/png">

	<title>Oops ! @yield('title')</title>

	<!-- Google font -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">

	<!-- Font Awesome icons -->
	<link href="{{ asset('assets/admin/js/plugins/@fortawesome/fontawesome-free/css/all.min.css') }}" rel="stylesheet" />


	<!-- Custom stlylesheet -->
	<style type="text/css">
		* {
			-webkit-box-sizing: border-box;
			box-sizing: border-box;
		}

		body {
			padding: 0;
			margin: 0;
			@if(date("m") == "12")
			background: linear-gradient(87deg, #5e72e4 0, #825ee4 100%) !important;
		    @endif
		}

		#notfound {
			position: relative;
			height: 100vh;
		}

		#notfound .notfound {
			position: absolute;
			left: 50%;
			top: 50%;
			-webkit-transform: translate(-50%, -50%);
			-ms-transform: translate(-50%, -50%);
			transform: translate(-50%, -50%);
		}

		.notfound {
			max-width: 520px;
			width: 100%;
			line-height: 1.4;
			text-align: center;
		}

		.notfound .notfound-404 {
			position: relative;
			height: 200px;
			margin: 0px auto 20px;
			z-index: -1;
		}

		.notfound .notfound-404 h1 {
			font-family: 'Open Sans', sans-serif;
			font-size: 236px;
			font-weight: 200;
			margin: 0px;
			@if(date("m") == "12")
			color: #fff;
			@else
			color: #211b19;
		    @endif
			text-transform: uppercase;
			position: absolute;
			left: 50%;
			top: 50%;
			-webkit-transform: translate(-50%, -50%);
			-ms-transform: translate(-50%, -50%);
			transform: translate(-50%, -50%);
		}

		.notfound .notfound-404 h2 {
			font-family: 'Open Sans', sans-serif;
			font-size: 28px;
			font-weight: 400;
			text-transform: uppercase;
			@if(date("m") == "12")
			color: #fff;
			background: #211b19;
			@else
			color: #211b19;
			background: #fff;
		    @endif
			padding: 10px 5px;
			margin: auto;
			display: inline-block;
			position: absolute;
			bottom: 0px;
			left: 0;
			right: 0;
		}

		.notfound a {
			font-family: 'Open Sans', sans-serif;
			display: inline-block;
			font-weight: 700;
			text-decoration: none;
			color: #fff;
			text-transform: uppercase;
			padding: 13px 23px;
			background-color:#5e72e4;
			font-size: 1rem;
			-webkit-transition: 0.2s all;
			transition: 0.2s all;
			border: 1px solid transparent;
			@if(date("m") == "12")
			border-color: #fff;
		    @endif
			box-shadow: 0 4px 6px rgba(50, 50, 93, 0.11), 0 1px 3px rgba(0, 0, 0, 0.08);
		}

		.notfound a:hover {
			@if(date("m") == "12")
			border-color:#5e72e4;
			color: #5e72e4;
			background: #fff;
			@else
			background: #fff;
			border-color:#5e72e4;
			color: #5e72e4;
		    @endif
		}

		@media only screen and (max-width: 767px) {
			.notfound .notfound-404 h1 {
				font-size: 148px;
			}
		}

		@media only screen and (max-width: 480px) {
			.notfound .notfound-404 {
				height: 148px;
				margin: 0px auto 10px;
			}
			.notfound .notfound-404 h1 {
				font-size: 86px;
			}
			.notfound .notfound-404 h2 {
				font-size: 16px;
			}
			.notfound a {
				padding: 7px 15px;
				font-size: 14px;
			}
		}

		@if(date("m") == "12")
		canvas {
            position: absolute;
            top: 0;
            left: 0;
            z-index: 0;
            width: 100%;
            height: 100%;
            pointer-events: none;
        }
		@endif
		

	</style>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

</head>

<body>
	<div id="notfound">
		<div class="notfound">
			<div class="notfound-404">
				<h1>Oops!</h1>
				<h2>@yield('code', __('Oh no')) - @yield('message')</h2>
			</div>
			<a href="{{ app('router')->has('home') ? route('home') : url('/') }}"><i class="fas fa-home"></i> {{ __('Go Home') }}</a>
		</div>
	</div>
	@if(date("m") == "12")
    <!-- Using a free CDN -->
    <script src="https://cdn.rawgit.com/scottschiller/Snowstorm/master/snowstorm-min.js"></script>
    <script type="text/javascript">
        // 1. Define a color for the snow
        snowStorm.snowColor = '#fff';
        // 2. To optimize, define the max number of flakes that can
        // be shown on screen at once
        snowStorm.flakesMaxActive = 96;
        // 3. Allow the snow to flicker in and out of the view
        snowStorm.useTwinkleEffect = true; 
        // 4. Start the snowstorm!
        snowStorm.start();
    </script>
    @else
    <canvas></canvas>
    <script type="text/javascript">
        document.addEventListener('touchmove', function (e) {
            e.preventDefault()
        })
        var c = document.getElementsByTagName('canvas')[0],
            x = c.getContext('2d'),
            pr = window.devicePixelRatio || 1,
            w = window.innerWidth,
            h = window.innerHeight,
            f = 60,
            q,
            m = Math,
            r = 0,
            u = m.PI*2,
            v = m.cos,
            z = m.random
        c.width = w*pr
        c.height = h*pr
        x.scale(pr, pr)
        x.globalAlpha = 0.5
        function i(){
            x.clearRect(0,0,w,h)
            q=[{x:0,y:h*.7+f},{x:0,y:h*.7-f}]
            while(q[1].x<w+f) d(q[0], q[1])
        }
        function d(i,j){
            x.beginPath()
            x.moveTo(i.x, i.y)
            x.lineTo(j.x, j.y)
            var k = j.x + (z()*2-0.25)*f,
                n = y(j.y)
            x.lineTo(k, n)
            x.closePath()
            r-=u/-50
            x.fillStyle = '#'+(v(r)*127+128<<16 | v(r+u/3)*127+128<<8 | v(r+u/3*2)*127+128).toString(16)
            x.fill()
            q[0] = q[1]
            q[1] = {x:k,y:n}
        }
        function y(p){
            var t = p + (z()*2-1.1)*f
            return (t>h||t<0) ? y(p) : t
        }
        document.onclick = i
        document.ontouchstart = i
        i()
	</script>
    @endif

</body>

</html>
