<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\Permission;

class PermissionRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	// Permission inside posts
		$list_posts_perm = new Permission();
		$list_posts_perm->slug = 'list-posts';
		$list_posts_perm->name = 'List Posts';
		$list_posts_perm->save();

		$add_posts_perm = new Permission();
		$add_posts_perm->slug = 'add-posts';
		$add_posts_perm->name = 'Add Posts';
		$add_posts_perm->save();

		$edit_posts_perm = new Permission();
		$edit_posts_perm->slug = 'edit-posts';
		$edit_posts_perm->name = 'Edit Posts';
		$edit_posts_perm->save();

		$delete_posts_perm = new Permission();
		$delete_posts_perm->slug = 'delete-posts';
		$delete_posts_perm->name = 'Delete Posts';
		$delete_posts_perm->save();

		// Permission between users
		$list_users_perm = new Permission();
		$list_users_perm->slug = 'list-users';
		$list_users_perm->name = 'List Users';
		$list_users_perm->save();

		$add_users_perm = new Permission();
		$add_users_perm->slug = 'add-users';
		$add_users_perm->name = 'Add Users';
		$add_users_perm->save();

		$edit_users_perm = new Permission();
		$edit_users_perm->slug = 'edit-users';
		$edit_users_perm->name = 'Edit Users';
		$edit_users_perm->save();

		$delete_users_perm = new Permission();
		$delete_users_perm->slug = 'delete-users';
		$delete_users_perm->name = 'Delete Users';
		$delete_users_perm->save();


		// RoleTableSeeder.php
		$user_role = new Role();
		$user_role->slug = 'user';
		$user_role->name = 'User';
		$user_role->description = 'A simple user of a plateforme';
		$user_role->save();

		$edit_role = new Role();
		$edit_role->slug = 'editor';
		$edit_role->name = 'Editor Manager';
		$edit_role->description = 'A person who is in charge of and determines the final content of a post';
		$edit_role->save();
		$edit_role->permissions()->attach($list_posts_perm);
		$edit_role->permissions()->attach($add_posts_perm);
		$edit_role->permissions()->attach($edit_posts_perm);
		$edit_role->permissions()->attach($delete_posts_perm);

		$admin_role = new Role();
		$admin_role->slug = 'admin';
		$admin_role->name = 'Admin Manager';
		$admin_role->description = 'A person in charge of managing system';
		$admin_role->save();
		$admin_role->permissions()->attach($list_posts_perm);
		$admin_role->permissions()->attach($add_posts_perm);
		$admin_role->permissions()->attach($edit_posts_perm);
		$admin_role->permissions()->attach($delete_posts_perm);

		$admin_role->permissions()->attach($list_users_perm);
		$admin_role->permissions()->attach($add_users_perm);
		$admin_role->permissions()->attach($edit_users_perm);

		$superadmin_role = new Role();
		$superadmin_role->slug = 'superadmin';
		$superadmin_role->name = 'Super Admin';
		$superadmin_role->description = 'A person in charge of managing all the system';
		$superadmin_role->save();
		$superadmin_role->permissions()->attach($list_posts_perm);
		$superadmin_role->permissions()->attach($add_posts_perm);
		$superadmin_role->permissions()->attach($edit_posts_perm);
		$superadmin_role->permissions()->attach($delete_posts_perm);

		$superadmin_role->permissions()->attach($list_users_perm);
		$superadmin_role->permissions()->attach($add_users_perm);
		$superadmin_role->permissions()->attach($edit_users_perm);
		$superadmin_role->permissions()->attach($delete_users_perm);

    }
}
