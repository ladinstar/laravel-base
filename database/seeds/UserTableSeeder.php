<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\Permission;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$user_role = Role::where('slug','user')->first();
		$edit_role = Role::where('slug','editor')->first();
	    $admin_role = Role::where('slug', 'admin')->first();
	    $superadmin_role = Role::where('slug', 'superadmin')->first();

	    $list_posts_perm = Permission::where('slug','list-posts')->first();
	    $add_posts_perm = Permission::where('slug','add-posts')->first();
	    $edit_posts_perm = Permission::where('slug','edit-posts')->first();
	    $delete_posts_perm = Permission::where('slug','delete-posts')->first();

	    $list_users_perm = Permission::where('slug','list-users')->first();
	    $add_users_perm = Permission::where('slug','add-users')->first();
	    $edit_users_perm = Permission::where('slug','edit-users')->first();
	    $delete_users_perm = Permission::where('slug','delete-users')->first();

	    $user = new User();
	    $user->name = 'Arsène Kaddanga';
	    $user->username = 'user';
	    $user->email = 'user@user.com';
	    $user->password = bcrypt('password');
	    $user->save();
	    $user->roles()->attach($user_role);

	    $editor = new User();
	    $editor->name = 'Alladin Avaïka';
	    $editor->username = 'editor';
	    $editor->email = 'editor@editor.com';
	    $editor->password = bcrypt('password');
	    $editor->save();
	    $editor->roles()->attach($edit_role);
		$editor->permissions()->attach($list_posts_perm);
		$editor->permissions()->attach($add_posts_perm);
		$editor->permissions()->attach($edit_posts_perm);
		$editor->permissions()->attach($delete_posts_perm);

	    $admin = new User();
	    $admin->name = 'Dominique';
	    $admin->username = 'admin';
	    $admin->email = 'admin@admin.com';
	    $admin->password = bcrypt('password');
	    $admin->save();
	    $admin->roles()->attach($admin_role);
		$admin->permissions()->attach($list_posts_perm);
		$admin->permissions()->attach($add_posts_perm);
		$admin->permissions()->attach($edit_posts_perm);
		$admin->permissions()->attach($delete_posts_perm);

		$admin->permissions()->attach($list_users_perm);
		$admin->permissions()->attach($add_users_perm);
		$admin->permissions()->attach($edit_users_perm);

		$superadmin = new User();
	    $superadmin->name = 'Dominique';
	    $superadmin->username = 'superadmin';
	    $superadmin->email = 'superadmin@superadmin.com';
	    $superadmin->password = bcrypt('password');
	    $superadmin->save();
	    $superadmin->roles()->attach($superadmin_role);
		$superadmin->permissions()->attach($list_posts_perm);
		$superadmin->permissions()->attach($add_posts_perm);
		$superadmin->permissions()->attach($edit_posts_perm);
		$superadmin->permissions()->attach($delete_posts_perm);

		$superadmin->permissions()->attach($list_users_perm);
		$superadmin->permissions()->attach($add_users_perm);
		$superadmin->permissions()->attach($edit_users_perm);
		$superadmin->permissions()->attach($delete_users_perm);
    }
}
