<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(
[
	'prefix' => 'admin',
	'as'=>'admin.',
	'middleware' => [
		'role:admin,superadmin,editor',
	]
], function() {

   	Route::get('dashboard', 'Admin\PageController@dashboard')->name('dashboard');
   	Route::get('/', 'Admin\PageController@dashboard')->name('index');

   	Route::resources([
    'users' => 'Admin\UserController',
	]);
	Route::get('users/{id}/active', 'Admin\UserController@active')->name('users.active');

	Route::resources([
    'roles' => 'Admin\RoleController',
	]);
	Route::resources([
    'permissions' => 'Admin\PermissionController',
	]);

});
